# Workflow Babel: Design document

## 1. Overview
Bioinformatics [suffers][1] [from][2] an abundance of _workflow tools_ or _pipeline frameworks_.
Unfortunately, none of these tools are designed to work together, so the community suffers from
a fragmentation of knowledge and an uphill battle (with software tools) for reproducible research.

In the `Unix` tradition, this project aims to do one thing and one thing well: to help glue these
disparate tools together by providing a common, visual mechanism to create (optimum) workflows
for any commonly used workflow tool. That is, a user may produce a workflow for any workflow
execution tool, and visually inspect and edit workflows produced for any workflow execution tool.
The workflows produced by these should be at high standard for readability, recommended useage
for the selected execution tool, and free from errors.

## 2. Context
There have been several attempts to standardise workflow languages, for example the
Common Workflow Language ([CWL]) and the Workflow Description Language ([WDL]). This tool is not
such a standardisation attempt. It merely attempts to provide a mechanism for existing tools,
and the people who work with them, to cooperate.

Furthermore, there exist tools such as [Rabix] for [CWL],... Often these tools allow the user
to *test* their workflow. This project may use static analysis and a "dry run" system to
aid the user in producing correct workflows without leaving the tool and suffering a potentially
long edit-test-fix cycle.

While the user of this system need not have a deep understanding of it, they should understand
the tools they are using *within* the workflow. This system does not assume to be an expert
on the variety of tools or platforms workflows may run on.

Within reasonable limits, this project does not assume the platform a user may wish to run
the projoct on.

## 3. Goals

1. A non-technical user can produce a potentially sophisticated visual workflow description 
suiting their problem.
2. A non-technical user can produce a textual workflow description that may be used with
their workflow tool of choice.
3. A non-technical user should trivially understand how to produce their workflow.
4. A novice user should be able to produce a sophisticated workflow without requiring a deep
understanding of the system to avoid subtle errors.
5. A user may run this project within a web-browser, on a desktop or laptop computer, or on
a mobile device.
6. This project should consume the minimum required CPU, memory, and network resources.

## 4. Non-Goals

1. The system will not execute workflows.
2. The system will not specify an implicit runtime environment (the user is responsible for providing
runtime dependencies through [Conda], [Nix], [Guix], [Docker], native packages, etc.).
3. This is not a "learning project" and no decisions are motivated by a desire to learn a given
   technology.

## 5. Milestones

* A visual editor
  * Visual design
  * Display nodes
  * Display edges between nodes
  * Editable node properties
  * Context menu
  * Editor menus
* Export system
  * Encoding of visual workflow (i.e. an _abstract representation_ or _AST_)
  * Translation into [SnakeMake]
  * Translation into [NextFlow]
* Import system
  * Decoding of an _abstract representation_ into something displayable.
  * Parsing [SnakeMake] files into the internal abstract representation.
  * Parsing of [NextFlow] files into the internal abstract representation.

## 6. Existing Solutions and Prior Art
"Visual programing" is not a new field. This section takes a cross-section type view of the
state-of-the-art from various application categories. Other, more thorough [review][10] of
this subject [here][11] and [here][12].

### 6.1 [Blender node editor][3]
Blender is a 3D multimedia tool. The node editor allows users to create shading programs as
graphs.

![Example blender nodes](images/blender_nodes_1.png)

The image above is a trivial example program which can be read from left-to-right. "Multiply
the values `1.500` and `0.100` and multiply the result by `1.100`."

The user can add nodes from the menu bar or using the key combination `Shift+A`. This displays the
following menu from which a user can select their desired node type (e.g. `Converter -> Math` for
the "`Multiply`" nodes in the previous example).

![Add a node, Blender](images/blender_add_node_menu.png)

Panning the graph of nodes around the screen can be achieved by holding `Shift+middle-mouse-button`.

Blender relies heavily on memorised keyboard shortcuts for efficient operation. Although the mouse
can be used quite efficiently for example to connect nodes without needing to accurately place
the connector endpoints requires holding `Alt+left-mouse-button` and dragging between the vincity
of the nodes you wish to connect.

_Connectors_ or _sockets_ are colour-coded points on each node to which an edge may be attached.
Sockets on the left-hand-side of a node are _inputs_, those on the right are _outputs_. The socket
colour indicates the type of data that must be fed to that socket, one of _colour_, _numeric_,
_vector_, or _shader_ data types.

The _properties_ within a node can be considered as arguments to the node function. The values
can be adjusted by clicking-and-sliding with the mouse or typing in an exact value:

![Blender node properties](https://docs.blender.org/manual/en/latest/_images/interface_controls_nodes_parts_controls.png)

A potentially useful feature for connecting new nodes involves dragging the new node over an edge:

![Blender auto layout](https://docs.blender.org/manual/en/latest/_images/interface_controls_nodes_arranging_auto-offset.png)

### 6.2 Unreal engine ["blueprint visual scripting language"][4]
"Blueprints" is an alternative method for writing general purpose programs in C++. It is intended
to be a fully-featured programming environment.

<img src="https://docs.unrealengine.com/Images/Engine/Blueprints/Editor/UIBreakdowns/LevelBPUI/LevelBlueprintUI.jp2"
    alt="Blueprint editor" style="width:100%;"/>

The following major interface elements are highlighted:
1. Main application menu.
2. Editor toolbar (provides an interface to compile the blueprint to `C++` and to run the blueprint
in the 3D scene).
3. Blueprint overview (contains a hierarchial breakdown of nodes, interface to create nodes
4. Context sensitive details area provides a mechanism for editing node properties of a selected node.
5. Main blueprint editor.

Nodes can be created from the blueprint overview or from a contextual right-click menu:

![Create blueprint nodes](https://docs.unrealengine.com/Images/Engine/Blueprints/Editor/UIComponents/MyBlueprint/myblueprint_rightclick.jp2)

Mouse and keyboard interface follows more commonly used interactions. Inputs and outputs are
clearly indicated by directional arrows, called "pins". Furthermore, connecting valid "pin"
types displays a green tick mark:

![Blueprint input output indicators](https://docs.unrealengine.com/Images/Engine/Blueprints/BP_HowTo/BasicUsage/BasicConnect.jp2)

Variable types are colour coded with `12` different colours. An example below feeds a Blue
`Object` typed variable into the `Light Color` property and an Orange `Transform` typed variable
into the `Spawn Transform` property:

![Blueprint variable types](https://docs.unrealengine.com/Images/Engine/Blueprints/UserGuide/Variables/HT27.jp2)

### 6.3 [Galaxy workflow editor][5]
The galaxy workflow editor is a web-based editor with some overlapping goals to this project:
a visual tool to create and share workflows for bioinformatics. Galaxy is also the runtime
environment and execution engine. It does not interact with other workflow tools.

Below is an annotated screenshot of the Galaxy workflow editor. Inputs to each node are in the
top-left and are clearly marked by an arrow pointing into the node, outputs are to the
bottom-right separated by a dotted horizontal line.

<img src="https://galaxyproject.org/images/learn/workflow_editor_overview_annotated.png"
    alt="Galaxy workflow editor" load="lazy" style="width:100%;"/>

Interacting with the graph is intuitive: left-click and drag to pan the display, left-click and
drag on the grey bar on each node to move it. Removing a _noodle_ from a _connector_ is as simple
of moving the mouse over a connector and clicking the button when it displays a `-` symbol (see below).

![A node in the Galaxy workflow editor](images/galaxy-node.png)

The green star marks intermediate results that are "saved".

### 6.4 [Knime][6]

Knime is a platform for data analytics.

![Knime workflow editor](https://docs.knime.com/latest/analytics_platform_quickstart_guide/img/02_knime_workbench.PNG)

### 6.5 [Rete][7]

Rete is "The javascript framework for visual programming"

![Rete nodes and edges](https://rete.js.org/img/preview.2f859fae.png)

### 6.6 [Scratch][8]
Scratch is an educational tool to teach children programming.

![Scratch hello world](https://upload.wikimedia.org/wikipedia/commons/7/78/Scratch_Hello_World.png)

### 6.7 [Orange][9]
This is a data mining and statistical modelling tool.

<img alt="Cross validation workflow" src="https://orange.biolab.si/workflow_images/cross-validation.png"
    load="lazy" style="width:100%;" />
    
The above screenshot is an editable workflow with annotations that can be formatted as plain text,
markdown, or restructured text. "Opening" a node opens a seperate window containing analysis data,
settings, etc.

### 6.8 [Fructure][14]

The Fructure code editor for [Racket][17] is an experimental "structural" editor allowing the
programmer to "explore the grammar" free from a requirement to type individial tokens in the
correct syntax.

<img alt="Using Fructure to explore the grammar"
    src="https://github.com/disconcision/fructure/raw/master/screenshots/walk-1.png"
    load="lazy" style="width:100%;" />

Notice the vertically scrolling menu in the screenshot above, another example appears below, it
is context sentitive and aware of the language grammar.

![Fructure menu closeup](https://github.com/disconcision/fructure/raw/master/screenshots/menu-2.png)

### 6.9 [Code Bubbles][15]

Code bubbles is a "front end to Eclipse designed to simplify programming by making it easy
for the programmer to define and use working sets. Working sets here consist of the group
of functions, documentation, notes, and other information that a programmer needs to
accomplish a particular programming task...".

A [video demonstration][16] perhaps best describes _code bubbles_. Nodes may be grouped together and
colour coded, they may display code, documentation, notes, test execution or debugging output.
Edges between nodes refer to their "child". For example, if function `A()` calls function `B()`
then the node containing the source code for `A()` will point to the node containing the source
code for `B()`. The edge will begin from the call site in `A()`:

<img alt="Code nodes in bubbles" src="https://cs.brown.edu/~spr/codebubbles/tutorial/tutorial_4.png"
    load="lazy" style="width:100%;"/>

This is similar to the way [Dr Racket][17] marks identifer uses when mousing over them
as can be seen in the example below:

![DrRacket marks identifier uses](images/drracket_identifier_marking.png)

## 7. Functional Requirements

* F1: 

## 8. User Interface Requirements

## 9. Non-functional Requirements

### 9.1 Performance

### 9.2 Security

### 9.3 Quality

## 10. Proposed Design

## 11. Alternative Designs

## 12. Open Questions

1. How should the language be abstractly represented? Some ideas may be found [here][13].

[@jhc_at_home]: https://github.com/jhc-at-home
[1]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5429012/
[2]: https://github.com/vibbits/example-bioinformatics-project
[3]: https://docs.blender.org/manual/en/latest/compositing/index.html
[4]: https://docs.unrealengine.com/en-US/Engine/Blueprints/index.html
[5]: https://galaxyproject.org/learn/advanced-workflow/basic-editing/
[6]: https://www.knime.com/knime
[7]: https://rete.js.org/
[8]: https://scratch.mit.edu/
[9]: https://orange.biolab.si/
[10]: https://www.craft.ai/blog/the-maturity-of-visual-programming
[11]: http://ksiresearchorg.ipage.com/vlss/journal/VLSS2017/vlss17paper_10.pdf
[12]: http://www.cs.ucf.edu/~dcm/Teaching/COT4810-Spring2011/Literature/DataFlowProgrammingLanguages.pdf
[13]: https://doi.org/10.1093%2Fcomjnl%2F44.3.186
[14]: https://fructure-editor.tumblr.com/
[15]: https://cs.brown.edu/~spr/codebubbles/
[16]: https://youtu.be/PsPX0nElJ0k
[17]: https://racket-lang.org/
[CWL]: https://www.commonwl.org/
[WDL]: https://openwdl.org/
[Rabix]: https://rabix.io
[SnakeMake]: https://snakemake.readthedocs.io/
[NextFlow]: https://www.nextflow.io/
[Conda]: https://docs.conda.io/en/latest/
[Nix]: https://nixos.org/nix/
[Guix]: https://guix.gnu.org/
[Docker]: https://www.docker.com/
