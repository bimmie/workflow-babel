# Workflow Babel
Create workflows graphically then output to your favourite execution systems. The "Workflow Babel"
name is a working title until I come up with something better.

## Supported execution systems
None are yet supported but systems commonly used in bioinformatics research environments
are expected to be first supported:

* SnakeMake
* Nextflow

## Design notes
Design notes can be found [here](doc/design.md).
